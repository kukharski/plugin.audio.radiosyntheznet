
import xbmc


URL = 'http://radio.synthez.net/play.m3u'


def playradio():
    pl = xbmc.PlayList(xbmc.PLAYLIST_MUSIC)
    pl.clear()
    pl.add(URL)
    xbmc.Player().play(pl)

playradio()
